package to.toodoo.core;

public interface AbstractEntity<T> {

  T getId();
}
