package to.toodoo.core;

import static java.util.UUID.randomUUID;

import java.util.UUID;
import javax.persistence.MappedSuperclass;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@MappedSuperclass
public abstract class BaseEntity<T> implements AbstractEntity<T> {

  private static final UUID uuid = randomUUID();

  @Override
  public abstract T getId();

  public UUID getUuid() {
    return uuid;
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder()
        .append(uuid)
        .toHashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    if (obj.getClass() != getClass()) {
      return false;
    }

    BaseEntity other = (BaseEntity) obj;

    return new EqualsBuilder()
        .append(uuid, other.getUuid())
        .isEquals();
  }
}
