package to.toodoo.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TooDooServerApplication {

  public static void main(String[] args) {
    SpringApplication.run(TooDooServerApplication.class, args);
  }
}
